package neu.mr.cs6240.a6_Validate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

/**
 * Class to compute final confusion matrix
 *
 * @author Smitha Bangalore Naresh
 * @author Ajay Subramanya
 *
 */
public class App {

	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Usage : <acutal file> <predicted file>");
			System.exit(-1);
		}

		String actfFile = args[0];
		String ourFile = args[1];

		fulldataConfusionMatrix(actfFile, ourFile);

		perMonthConfusionMatrix(actfFile, ourFile);
	}

	/**
	 * Computes confusion Matrix per Month data
	 *
	 * @param actfFile
	 * @param ourFile
	 */
	private static void perMonthConfusionMatrix(String actfFile, String ourFile) {

		Map<String, String> actP = readGZipFile(actfFile);

		Map<Integer, Table<String, String, Integer>> perMonth = new HashMap<>();
		for (int i = 1; i <= 12; i++) {
			Table<String, String, Integer> confusionM = HashBasedTable.create();
			perMonth.put(i, confusionM);
		}

		try (BufferedReader br = new BufferedReader(new FileReader(new File(ourFile)))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] ourP = sanity(line);

				if (ourP != null && ourP.length < 2) continue;
				String[] key = ourP[0].split("_");

				try {
					String[] date = key[1].split("-");
					int month = Integer.parseInt(date[1]);

					if (actP.containsKey(ourP[0])) {
						String value1 = actP.get(ourP[0]).trim();
						String value2 = ourP[1].trim();
						perMonth.get(month).put(value1, value2, getVal(value1, value2, perMonth.get(month)));
						// To remove duplicate entries
						actP.remove(ourP[0]);
					}
				} catch (NumberFormatException e) {
					// Do nothing
				}

			}

			printResultsPerMonth(perMonth);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private static void printResultsPerMonth(Map<Integer, Table<String, String, Integer>> perMonth) {
		for (Integer i = 1; i <= 12; i++) {
			printResults(perMonth.get(i), "For Month " + i.toString());
		}

	}

	/**
	 * Prints confusion matrix for full data
	 *
	 * @param actfFile
	 * @param ourFile
	 */
	private static void fulldataConfusionMatrix(String actfFile, String ourFile) {
		Map<String, String> actP = readGZipFile(actfFile);

		Table<String, String, Integer> confusion = HashBasedTable.create();
		try (BufferedReader br = new BufferedReader(new FileReader(new File(ourFile)))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] ourP = sanity(line);

				if (ourP != null && ourP.length < 2) continue;

				if (actP.containsKey(ourP[0])) {
					String value1 = actP.get(ourP[0]).trim();
					String value2 = ourP[1].trim();

					confusion.put(value1, value2, getVal(value1, value2, confusion));
					// To remove duplicate entries
					actP.remove(ourP[0]);
				}
			}

			printResults(confusion, "For Full Data");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Prints the confusion matrix and percentage of error to console as well as
	 * file
	 *
	 * @param confusion
	 */
	private static void printResults(Table<String, String, Integer> confusion, String msg) {
		if (confusion.get("TRUE", "TRUE") == null || confusion.get("TRUE", "FALSE") == null
				|| confusion.get("FALSE", "TRUE") == null || confusion.get("FALSE", "FALSE") == null)
			return;

		System.out.println("Confusion Matrix " + msg);
		System.out.println("\tTRUE \t\t FALSE");
		System.out.println("TRUE\t" + confusion.get("TRUE", "TRUE") + "\t\t" + confusion.get("TRUE", "FALSE"));
		System.out.println("FALSE\t" + confusion.get("FALSE", "TRUE") + "\t\t" + confusion.get("FALSE", "FALSE"));
		long sumCorrect = confusion.get("TRUE", "TRUE") + confusion.get("FALSE", "FALSE");
		long sumError = +confusion.get("TRUE", "FALSE") + confusion.get("FALSE", "TRUE");
		double correctP = (sumCorrect * 1.0 / (sumCorrect + sumError));
		double wrongP = (sumError * 1.0 / (sumCorrect + sumError));

		System.out.println("\nCorrect Prediction :" + correctP);
		System.out.println("Error Prediction :" + wrongP);

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(new File("ConfusionMatrix.txt"), true))) {

			bw.write("\nConfusion Matrix " + msg + "\n");
			bw.write("\tTRUE \t\t FALSE\n");
			bw.write("TRUE\t" + confusion.get("TRUE", "TRUE") + "\t\t" + confusion.get("TRUE", "FALSE") + "\n");
			bw.write("FALSE\t" + confusion.get("FALSE", "TRUE") + "\t\t" + confusion.get("FALSE", "FALSE") + "\n");

			bw.write("\nCorrect Prediction :" + correctP + "\n");
			bw.write("Error Prediction :" + wrongP + "\n");

		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Increments value in HashTable for given row, column
	 *
	 * @param value1
	 * @param value2
	 * @param confusion
	 * @return
	 */
	private static int getVal(String value1, String value2, Table<String, String, Integer> confusion) {
		return confusion.contains(value1, value2) ? confusion.get(value1, value2) + 1 : 1;
	}

	/**
	 * Creates a map for actual validate data
	 *
	 * @param file
	 * @return
	 */
	private static Map<String, String> readGZipFile(String file) {
		Map<String, String> m = new HashMap<String, String>();

		String sCurrentLine;
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new GZIPInputStream(new FileInputStream(file))))) {
			while ((sCurrentLine = br.readLine()) != null) {
				if (sCurrentLine.length() <= 0) continue;
				String[] s = sanity(sCurrentLine);
				if (s != null && s.length == 2) {
					m.put(s[0], s[1]);
				}
			}

		} catch (ZipException e) {
			System.err.println(file + " Not in GZIP format");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return m;
	}

	/**
	 * Checks the lines for sanity
	 *
	 * @param line
	 * @return
	 */
	private static String[] sanity(String line) {
		String[] s = line.split(",");
		if (s.length == 2) {
			String[] key = s[0].split("_");
			if (key.length == 3 && s[1] != null) {
				// Do sanity
				try {
					@SuppressWarnings("unused")
					int flNum = Integer.parseInt(key[0]);
					@SuppressWarnings("unused")
					int crsDepTime = Integer.parseInt(key[2]);
					SimpleDateFormat sd = new SimpleDateFormat("YYYY-MM-DD");
					@SuppressWarnings("unused")
					Date cur = sd.parse(key[1]);
					@SuppressWarnings("unused")
					boolean pred = Boolean.parseBoolean(s[1]);
				} catch (NumberFormatException | ParseException e) {
					return null;
				}
			}
		}
		return s;
	}
}
